import 'package:crudapp/model/clientmodel.dart';
import 'package:crudapp/register.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListaCliente extends StatefulWidget {
  final Cliente cliente;

  ListaCliente({this.cliente});

  @override
  _ListaClienteState createState() => _ListaClienteState();
}

class _ListaClienteState extends State<ListaCliente> {
  ClienteHelper helper = ClienteHelper();

  List<Cliente> clientes = List();

  @override
  void initState() {
    super.initState();

    _getAllClientes();
  }

  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text("Cadastrados"),
      ),
      child: SafeArea(
        child: Scaffold(
          body: _listInside(),
        ),
      ),
    );
  }

  //Widget para construir a tela
  Widget _listInside() {
    return ListView.builder(
        padding: EdgeInsets.all(10),
        itemCount: clientes.length,
        itemBuilder: (context, index) {
          return _clienteCard(context, index);
        });
  }

  //Card do usuário
  Widget _clienteCard(BuildContext context, int index) {
    return GestureDetector(
        child: Column(
          children: <Widget>[
            Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.fromLTRB(300, 0, 0, 0),
                      child: CupertinoButton(
                          child: Icon(
                            CupertinoIcons.delete,
                            size: 30,
                            color: Colors.red,
                          ),
                          onPressed: () {
                            alerta(context, index);
                          })),
                  ListTile(
                    leading: Icon(
                      CupertinoIcons.check_mark_circled,
                      size: 45,
                    ),
                    title: Text(
                      clientes[index].nome,
                      style:
                          TextStyle(fontSize: 23, fontWeight: FontWeight.w700),
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Padding(padding: EdgeInsets.fromLTRB(75, 0, 0, 0)),
                          Text(
                            "Email:   ",
                            style: TextStyle(fontSize: 20),
                          ),
                          Text(
                            clientes[index].email,
                            style: TextStyle(fontSize: 17),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Padding(padding: EdgeInsets.fromLTRB(75, 0, 0, 0)),
                          Text(
                            "Contato:   ",
                            style: TextStyle(fontSize: 20),
                          ),
                          Text(
                            clientes[index].fone,
                            style: TextStyle(fontSize: 17),
                          ),
                        ],
                      ),
                    ],
                  ),
                  ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text(
                            'Clique no Card para editar informações'),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        onTap: () {
          showClientePage(cliente: clientes[index]);
        });
  }

  //Retorna um alerta de "segurança" para não excluir de uma vez o cliente.
  Future<void> alerta(BuildContext context, int index) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text("Excluir o cliente: " + clientes[index].nome + "?"),
          content: Text("Esta ação é permanente !."),
          actions: <Widget>[
            CupertinoDialogAction(
                child: Text('Sim, excluir.', style: TextStyle(color: Colors.red),),
                onPressed: () {
                  helper.delCliente(clientes[index].id);
                  Navigator.pop(context);
                  setState(() {
                    clientes.removeAt(index);
                  });
                }),
            CupertinoDialogAction(
              child: Text('Não.'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  /*Para redirecionar para a pagina do cliente.
  aqui, caso o cliente não estiver nulo,
  é realizado o update e recupera o cliente atualizado na lista
  (Mesma logica usada para o cadastro)*/
  void showClientePage({Cliente cliente}) async {
    final recCliente = await Navigator.push(
        context,
        CupertinoPageRoute(
            builder: (context) => Register(
                  cliente: cliente,
                )));
    if (recCliente != null) {
      if (cliente != null) {
        await helper.updateCliente(recCliente);
      }
      _getAllClientes();
    }
  }

  void _getAllClientes() {
    helper.getAllClientes().then((list) {
      setState(() {
        clientes = list;
      });
    });
  }
}
