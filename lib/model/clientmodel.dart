import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:scoped_model/scoped_model.dart';

final String clienteTable = "clienteTable";
final String idColumn = "idColumn";
final String nomeColumn = "noneColumn";
final String senhaColumn = "senhaColumn";
final String idadeColumn = "idadeColumn";
final String emailColumn = "emailColumn";
final String foneColumn = "foneColumn";



class ClienteHelper extends Model{

  static final ClienteHelper _instance = ClienteHelper.internal();

  factory ClienteHelper() =>_instance;

  ClienteHelper.internal();

  Database _db;

  Future<Database> get db async {
    if(_db != null){
      return _db;
    }else{
      _db = await initDb();
      return _db;
    }
  }

Future<Database> initDb() async {
  final databasePath = await getDatabasesPath();
  final path = join(databasePath, "clientes.db");

 return await openDatabase(path, version: 1, onCreate: (Database db, int newerVersion) async {
    await db.execute(
      "CREATE TABLE $clienteTable($idColumn INTEGER PRIMARY KEY, $nomeColumn TEXT, $senhaColumn, $idadeColumn TEXT, $emailColumn TEXT, $foneColumn TEXT)"
    );
  });
}

  //Salvando o Cliente noo banco
  Future<Cliente> inserirCliente(Cliente cliente)async{
    Database dbCliente = await db;
    cliente.id = await dbCliente.insert(clienteTable, cliente.toMap());
    return cliente;
  }
  
  //Obtendo um cliente pelo id
  Future<Cliente> getCliente(int id)async{
    Database dbCliente = await db;
    List<Map>maps = await dbCliente.query(clienteTable, 
    columns: [idColumn, nomeColumn, idadeColumn, emailColumn],
    where: "$idColumn =?",
    whereArgs: [id]);
    if(maps.length > 0){
      return Cliente.fromMap(maps.first);
    }return null;
  }

  //Deletar Cliente
  Future<int> delCliente(int id)async{
    Database dbCliente = await db;
    return await dbCliente.delete(clienteTable, where: "$idColumn = ?", whereArgs: [id]);
  }

  //Atualizar os dados do cliente
  Future<int>updateCliente(Cliente cliente)async{
    Database dbCliente = await db;
    return await dbCliente.update(clienteTable, cliente.toMap(), where: "$idColumn = ?", whereArgs: [cliente.id]);
  }

  //Pegar todos os clientes
  Future<List>getAllClientes()async{
    Database dbCliente = await db;
    List listMap = await dbCliente.rawQuery("SELECT * FROM $clienteTable");
    List<Cliente> listCliente = List();
    for(Map m in listMap){
      listCliente.add(Cliente.fromMap(m));
    }
    return listCliente;
  }

  //Pegar o numero de clientes cadastrados
  Future<int>getNumeroCli()async {
    Database dbCliente = await db;
    return Sqflite.firstIntValue(await dbCliente.rawQuery("SELECT COUNT(*) FROM $clienteTable"));
  }

  //Fechar o banco de dados
  Future close()async{
    Database dbCliente = await db;
    dbCliente.close();
  }


}

class Cliente extends Model{

  int id;
  String nome;
  String senha;
  String idade;
  String email;
  String fone;
  

  Cliente _editedCliente;

  Cliente();

  Cliente.fromMap(Map map){
    id = map[idColumn];
    nome = map[nomeColumn];
    senha = map[senhaColumn];
    idade = map[idadeColumn];
    email = map[emailColumn];
    fone = map[foneColumn];
   
  }

  Map toMap(){
    Map<String, dynamic>map = {
      nomeColumn: nome,
      senhaColumn: senha,
      idadeColumn: idade,
      emailColumn: email,
      foneColumn: fone,
    };
    if(id != null){
      map[idColumn] =id;
    }
    return map;
  }
  @override
  String toString(){
    return "Cliente(id: $id, nome: $nome, senha: $senha, idade: $idade, email: $email, fone: $fone)";
  }
}


