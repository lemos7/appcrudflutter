import 'package:flutter/material.dart';
import 'login_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CRUD APP',
      
      theme: ThemeData(
        primarySwatch: Colors.cyan,
        //primaryColor: Color.fromARGB(255, 117, 40, 212)
        
      ),
      debugShowCheckedModeBanner: false,
      home: LoginScreen()
    );
  }
}
