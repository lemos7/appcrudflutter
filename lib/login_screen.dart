import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'model/clientmodel.dart';
import 'register.dart';
import 'list.dart';

class LoginScreen extends StatefulWidget {
  final Cliente cliente;

  LoginScreen({this.cliente});

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text("Agenda de contatos"),
      ),
      child: SafeArea(
          child: SingleChildScrollView(
        child: loginInside(context),
      )),
    );
  }

  Widget loginInside(BuildContext context) {
    Cliente cliente;

    return CupertinoPageScaffold(
        child: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(height: 30.0),
          Container(
              alignment: Alignment.center,
              child: Icon(
                CupertinoIcons.group,
                size: 120,
              )),
          SizedBox(height: 40),
          Container(
            width: 300,
            height: 35,
            child: CupertinoTextField(
              placeholder: "Email",
              style: TextStyle(fontSize: 20),
            ),
          ),
          SizedBox(height: 20.0),
          Container(
            width: 300,
            height: 35,
            child: CupertinoTextField(
              placeholder: "Senha",
              style: TextStyle(fontSize: 18),
            ),
          ),
          SizedBox(height: 30.0),
          Container(
              /*width: 160,
                height: 60,*/
              child: CupertinoButton(
            borderRadius: BorderRadius.circular(10),
            color: Colors.cyan,
            child: Text(
              "  Login  ",
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
            onPressed: () {
              Navigator.push(
                context,
                CupertinoPageRoute(builder: (context) => ListaCliente()),
              );
            },
          )),
          SizedBox(height: 110.0),
          Container(
              child: CupertinoButton(
                  child: Text("Não possui um cadastro ? Então faça agora !"),
                  onPressed: null)),
          Container(
              child: CupertinoButton(
                  borderRadius: BorderRadius.circular(10),
                  //color: Colors.deepPurple,
                  child: Text(
                    "  Cadastrar  ",
                    style: TextStyle(fontSize: 15, color: Colors.blue),
                  ),
                  onPressed: () {
                    showClientePage();
                  })),
        ],
      ),
    ));
  }

  ClienteHelper helper = ClienteHelper();
  List<Cliente> clientes = List();
  
  void showClientePage({Cliente cliente}) async {
    final recCliente = await Navigator.push(
        context,
        CupertinoPageRoute(
            builder: (context) => Register(
                  cliente: cliente,
                )));
    if (recCliente != null) {
      if (cliente != null) {
        await helper.updateCliente(recCliente);
      } else {
        await helper.inserirCliente(recCliente);
      }
      _getAllClientes();
    }
  }

  void _getAllClientes() {
    helper.getAllClientes().then((list) {
      setState(() {
        clientes = list;
      });
    });
  }
}
