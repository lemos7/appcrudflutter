import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'model/clientmodel.dart';

final _nomeController = TextEditingController();
final _senhaController = TextEditingController();
final _idadeController = TextEditingController();
final _emailController = TextEditingController();
final _foneController = TextEditingController();

void resetTF() {
  _nomeController.text = "";
  _senhaController.text = "";
  _idadeController.text = "";
  _emailController.text = "";
  _foneController.text = "";
}

class Register extends StatefulWidget {
  final Cliente cliente;

  Register({this.cliente});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  ClienteHelper helper = ClienteHelper();

  Cliente cliente = Cliente();

  /*Para mostrar e ocultar a senha */
  /* bool invisible = true;
  void inContact(TapDownDetails details) {
    setState(() {
      invisible = false;
    });
  }

  void outContact(TapUpDetails details) {
    setState(() {
      invisible = true;
    });
  }
*/

  Cliente _editedCliente;

  @override
  void initState() {
    super.initState();
    if (widget.cliente == null) {
      _editedCliente = Cliente();
    } else {
      _editedCliente = Cliente.fromMap(widget.cliente.toMap());

      _nomeController.text = _editedCliente.nome;
      _senhaController.text = _editedCliente.senha;
      _idadeController.text = _editedCliente.idade;
      _emailController.text = _editedCliente.email;
      _foneController.text = _editedCliente.fone;
    }
  }

  bool _userEdited = false;

  final _focusName = FocusNode();

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text("Cadastro"),
      ),
      child: SafeArea(
          child: SingleChildScrollView(
        child: _screenInside(context),
      )),
    );
  }

  Widget _screenInside(BuildContext context) {
    return SingleChildScrollView(
      child: CupertinoPageScaffold(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 30.0),
            Container(
                alignment: Alignment.center,
                child: Icon(
                  CupertinoIcons.person_add,
                  size: 120,
                )),
            SizedBox(height: 40),

            //NOME
            Container(
              width: 300,
              height: 30,
              child: CupertinoTextField(
                controller: _nomeController,
                focusNode: _focusName,
                onChanged: (text) {
                  _userEdited = true;
                  _editedCliente.nome = text;
                },
                placeholder: "NOME COMPLETO",
                style: TextStyle(fontSize: 18),
              ),
            ),
            SizedBox(height: 20),
            //SENHA
            Container(
              width: 300,
              height: 30,
              child: CupertinoTextField(
                onChanged: (text) {
                  _userEdited = true;
                  _editedCliente.senha = text;
                },
                controller: _senhaController,
                placeholder: "SENHA",
                style: TextStyle(
                  fontSize: 18,
                ),
                keyboardType: TextInputType.visiblePassword,
              ),
            ),
            SizedBox(height: 20),
            Container(
              width: 300,
              height: 30,
              child: CupertinoTextField(
                onChanged: (text) {
                  _userEdited = true;
                  _editedCliente.idade = text;
                },
                controller: _idadeController,
                placeholder: "IDADE",
                style: TextStyle(fontSize: 18),
                keyboardType: TextInputType.number,
              ),
            ),

            //IGNORAR
            //SENHA
            /*Row(
              children: <Widget>[
                SizedBox(width: 3),
                GestureDetector(
                  onTapDown: inContact, //call this method when incontact
                  onTapUp:
                      outContact, //call this method when contact with screen is removed
                  child: Icon(
                    Icons.remove_red_eye,
                    color: Colors.red,
                  ),
                ),
                SizedBox(width: 15),
                Container(
                  width: 300,
                  height: 30,
                  child: CupertinoTextField(
                    style: TextStyle(fontSize: 18,),
                    keyboardType: TextInputType.visiblePassword,
                  ),
                ),
                
                
              ],
            ),*/
            SizedBox(height: 20),
            //EMAIL
            Container(
              width: 300,
              height: 30,
              child: CupertinoTextField(
                controller: _emailController,
                onChanged: (text) {
                  _userEdited = true;
                  _editedCliente.email = text;
                },
                placeholder: "EMAIL PESSOAL",
                style: TextStyle(fontSize: 18),
                keyboardType: TextInputType.emailAddress,
              ),
            ),
            SizedBox(height: 20),
            //FONE
            Container(
              width: 300,
              height: 30,
              child: CupertinoTextField(
                controller: _foneController,
                onChanged: (text) {
                  _userEdited = true;
                  _editedCliente.fone = text;
                },
                placeholder: "TELEFONE PARA CONTATO",
                style: TextStyle(fontSize: 18),
                keyboardType: TextInputType.number,
              ),
            ),
            SizedBox(height: 35),
            Container(
                /*width: 160,
                height: 60,*/
                child: CupertinoButton(
              borderRadius: BorderRadius.circular(10),
              color: Colors.cyan,
              child: Text(
                "  Concluído  ",
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              onPressed: () {
                
                if (_editedCliente.nome != null  && _editedCliente.senha != null &&
                _editedCliente.idade != null && _editedCliente.email != null &&
                _editedCliente.fone != null && _editedCliente.nome.isNotEmpty &&
                _editedCliente.senha.isNotEmpty && _editedCliente.idade.isNotEmpty &&
                _editedCliente.email.isNotEmpty && _editedCliente.fone.isNotEmpty) {
                  Navigator.pop(context, _editedCliente);
                  resetTF();
                } else {
                  alerta(context);
                 // FocusScope.of(context).requestFocus(_focusName);
                }
                
              },
            )),
          ],
        ),
      ),
    );
  }
}

Future<void> alerta(BuildContext context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return CupertinoAlertDialog(
        title: Text('Você preencheu todos os campos ?'),
        content: Text('para que o app funcione corretamente é preciso que voce cadastre todas as informações nos campos do usuário.'),
        actions: <Widget>[
          CupertinoDialogAction(
            child: Text('OK, vou revisar'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
